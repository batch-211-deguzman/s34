const express = require("express");

const app = express()

// For our application server to run, we need a port to listen top

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended: true}));

app.get('/', (request, response) => {
	response.send('Hello World')
})

app.get('/home', (request, response) => {
	response.send('Welcome to the home page')
})


app.post('/users', (request, response) => {
	response.send(`Hello there, ${request.body.username}! This is from the "/users endpoint but with a post method`)
})

let users = [{
			"username": "Mary",
			"password": "lol"
		},
		{
			"username": "John",
			"password": "haha"
		}]

app.post('/users', (request, response) => { 
	if (request.body.username !== '' && request.body.password !== '') {
		users.push(request.body)
		response.send(`User ${request.body.username} successfully registered!`)
		console.log(request.body)
	} else {
		response.send('Please input BOTH username and passowrd')
	}
})

app.get('/users', (request, response) => {
	response.json(users)
})

// For fun, I just added get and post under /delete-user
app.post('/delete-user', (request, response) => {
	response.send(`Hello there, ${request.body.username}! This is from the "/users endpoint but with a post method`)
})

let user = [{
			"username": "Maria",
			"password": "bwahahah"
		},
		{
			"username": "Peter",
			"password": "yey"
		}]

app.post('/delete-user', (request, response) => { 
	if (request.body.username !== '' && request.body.password !== '') {
		user.push(request.body)
		response.send(`User ${request.body.username} successfully registered!`)
		console.log(request.body)
	} else {
		response.send('Please input BOTH username and passowrd')
	}
})

app.get('/delete-user', (request, response) => {
	response.json(user)
})

app.delete('/delete-user', (request, response) => {
  response.send("The user has been deleted")
})


app.listen(port, () => console.log(`Server running at port ${port}`))
